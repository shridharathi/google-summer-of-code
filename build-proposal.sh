#!/bin/bash

mkdir -p build/`date +%Y`-proposals/

in=$1
out=`basename $in`
out=build/`date +%Y`-proposals/${out%md}pdf

echo "Converting $in to $out"

#iconv -t utf-8 $in | 
pandoc -s --metadata=link-citations:true --citeproc \
    -f gfm \
    -t pdf --pdf-engine=xelatex \
    -V 'mainfont:DejaVuSerif.ttf' \
    -V 'sansfont:DejaVuSans.ttf' \
    -V 'monofont:DejaVuSansMono.ttf' \
    -V 'mathfont:texgyredejavu-math.otf' \
    -V 'geometry:margin=5mm' \
    $in -o $out
# Bibliographies are listed in report.md metadata
# Alternatively they can be passed as parameters to pandoc
# --bibliography 1.bib --bibliography file2.bib # ...